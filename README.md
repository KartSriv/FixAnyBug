<h1 align="center">
  <br>
  <a href="https://github.com/KartSriv/FixAnyBug/"><img src="https://www.shareicon.net/data/128x128/2016/08/18/813383_computer_512x512.png" alt="Markdownify" width="200"></a>
</h1>

<h1 align="center">FixAnyBug</h1>
<h4 align="center">The last tool you need to fix a bug <a href="https://en.wikipedia.org/wiki/Joke" target="_blank">[JOKE]</a></h4>
<h4 align="center"><a href="https://github.com/KartSriv/FixAnyBug/releases/download/Alpha/FixAnyBug_Linux.sh">Linux</a> • <a href="https://github.com/KartSriv/FixAnyBug/releases/download/Alpha/FixAnyBug_Windows.bat">Windows</a> • <a href="https://github.com/KartSriv/FixAnyBug/releases/download/Alpha/FixAnyBug_macOS.sh">macOS</a> • <a href="https://github.com/KartSriv/FixAnyBug/releases/tag/Alpha">Releases</a><br>
  
 
<br>
<a href="https://saythanks.io/to/KartSriv"><img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg"><a> <img src="https://img.shields.io/github/license/KartSriv/FixAnyBug.svg"><a> <img src="https://img.shields.io/github/downloads/KartSriv/FixAnyBug/total.svg"></a>
<br>
<br>
<img src="http://ForTheBadge.com/images/badges/built-with-science.svg">
<img src="http://ForTheBadge.com/images/badges/built-with-love.svg">
</h4>
<br>
<br>

# The Revolutionary Algorithm
The Script uses the revolutionary technology known as **Dynamic Inputting System** also known as **(DIS)** to accept the Buggy file the user wants to fix and creates a **TSB** **(Temporary Backing Storage)** in Root Directory of the user and uses the **most efficient way to replicate** the Buggy File after that it swiftly **destroyed** the whole file resulting in a bug-free file.
<br>
<br>
<h1 align="center">
<img src="https://i.imgur.com/oIABf9O.png">
</h1>

**IN SIMPLE WORDS:** It just fricken deletes your file and creates a empty file with the same name.
