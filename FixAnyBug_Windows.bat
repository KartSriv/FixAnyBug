@ECHO OFF

ECHO Welcome to Fix Any Bug!
SET /P buggyFile=Path of File you want to debug: 
md C:\FixAnyBug\Temp\
xcopy /s %buggyFile% C:\FixAnyBug\Temp\
del /s %buggyFile%
copy /y NUL %buggyFile% >NUL
ECHO "Debug Completed!"
